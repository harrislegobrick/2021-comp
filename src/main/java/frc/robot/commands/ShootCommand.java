// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import static edu.wpi.first.wpilibj.Timer.getFPGATimestamp;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Limelight;

public class ShootCommand extends CommandBase {
	private final Flywheel flywheel;
	private final Limelight limelight;
	private final Indexer indexer;
	private final double delay = 0.5;
	private final double rpm = 4000;
	private double initTime;

	/** Creates a new ShootCommand. */
	public ShootCommand(Flywheel flywheel, Limelight limelight, Indexer indexer) {
		// Use addRequirements() here to declare subsystem dependencies.
		this.flywheel = flywheel;
		this.limelight = limelight;
		this.indexer = indexer;
		addRequirements(flywheel, limelight, indexer);
	}

	// Called when the command is initially scheduled.
	@Override
	public void initialize() {
		initTime = getFPGATimestamp();
		limelight.setTracking();
	}

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		flywheel.setRPM(rpm);

		// give a bit of time to let the wheel spin up and in the mean time reverse
		if (getFPGATimestamp() > initTime + delay) {

			// stop the indexer while the rpm isn't high enough to shoot
			if (flywheel.getVelocity() > rpm * 0.95) {
				indexer.run();
			} else {
				indexer.stop();
			}

		} else {
			indexer.reverse();
		}
	}

	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		flywheel.stop();
		indexer.stop();
		limelight.setDriving();
	}

	// Returns true when the command should end.
	@Override
	public boolean isFinished() {
		return false;
	}
}
