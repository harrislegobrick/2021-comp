// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autons;

import java.util.List;

import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.constraint.CentripetalAccelerationConstraint;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants.kDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.util.path.RamseteCommandFollower;
import frc.robot.util.path.TrajectoryHelper;

public class BounceRun extends SequentialCommandGroup {
	private final double maxAcceleration = Units.feetToMeters(4);
	private final double maxVelocity = Units.feetToMeters(6);
	private final double maxCentripitalAcceleration = Units.feetToMeters(2);
	private final double endVelocity = Units.feetToMeters(4);

	/** Creates a new BounceRun. */
	public BounceRun(Drivetrain drivetrain) {
		TrajectoryConfig forwardsConfig = new TrajectoryConfig(maxVelocity, maxAcceleration);
		forwardsConfig.setKinematics(kDrivetrain.DRIVE_KINEMATICS);
		forwardsConfig.setEndVelocity(endVelocity);
		forwardsConfig.addConstraints(List.of(kDrivetrain.autoVoltageConstraint,
				new CentripetalAccelerationConstraint(maxCentripitalAcceleration)));

		TrajectoryConfig backwardsConfig = new TrajectoryConfig(maxVelocity, maxAcceleration);
		backwardsConfig.setKinematics(kDrivetrain.DRIVE_KINEMATICS);
		backwardsConfig.setEndVelocity(endVelocity);
		backwardsConfig.setReversed(true);
		backwardsConfig.addConstraints(List.of(kDrivetrain.autoVoltageConstraint,
				new CentripetalAccelerationConstraint(maxCentripitalAcceleration)));

		Trajectory trajectory1 = TrajectoryHelper.generateTrajectory("Bounce1", forwardsConfig);
		Trajectory trajectory2 = TrajectoryHelper.generateTrajectory("Bounce2", backwardsConfig);
		Trajectory trajectory3 = TrajectoryHelper.generateTrajectory("Bounce3", forwardsConfig);
		Trajectory trajectory4 = TrajectoryHelper.generateTrajectory("Bounce4", backwardsConfig);

		RamseteCommandFollower run1 = new RamseteCommandFollower(trajectory1, drivetrain);
		RamseteCommandFollower run2 = new RamseteCommandFollower(trajectory2, drivetrain).startAtTrajectoryStart(false);
		RamseteCommandFollower run3 = new RamseteCommandFollower(trajectory3, drivetrain).startAtTrajectoryStart(false);
		RamseteCommandFollower run4 = new RamseteCommandFollower(trajectory4, drivetrain).startAtTrajectoryStart(false);

		addCommands(run1, run2, run3, run4);
	}
}
