// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autons;

import java.util.List;

import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.constraint.CentripetalAccelerationConstraint;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants.kDrivetrain;
import frc.robot.subsystems.Drivetrain;
import frc.robot.util.path.RamseteCommandFollower;
import frc.robot.util.path.TrajectoryHelper;

public class SlalomPath extends SequentialCommandGroup {
    private final double maxAcceleration = Units.feetToMeters(4);
    private final double maxVelocity = Units.feetToMeters(8);
    private final double maxCentripitalAcceleration = Units.feetToMeters(2);
    private final double endVelocity = Units.feetToMeters(2);

    /** Creates a new SlalomPath. */
    public SlalomPath(Drivetrain drivetrain) {
        TrajectoryConfig config = new TrajectoryConfig(maxVelocity, maxAcceleration);
        config.setKinematics(kDrivetrain.DRIVE_KINEMATICS);
        config.setEndVelocity(endVelocity);
        config.addConstraints(List.of(kDrivetrain.autoVoltageConstraint,
                new CentripetalAccelerationConstraint(maxCentripitalAcceleration)));

        Trajectory slalomPathTrajGen = TrajectoryHelper.generateTrajectory("Slalom", config);

        addCommands(new RamseteCommandFollower(slalomPathTrajGen, drivetrain));
    }
}
