// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final class kDrivetrain {
        public static final double WHEEL_DIAMETER = Units.inchesToMeters(6);
        public static final double WHEEL_CIRCUMFERENCE = WHEEL_DIAMETER * Math.PI;
        public static final int ENCODER_CPR = 4096; // SRX mag encoder is 4096, Falcon 500 is 2048

        // 1 because it's on the output shaft. Change this to gearbox ratio if not on
        // output shaft (on kitframe this number would be 10.71)
        public static final double GEAR_RATIO = 1;

        /**
         * Converts the native unit of the motor's encoder (CPR) to meters.
         */
        public static final double TICKS_TO_METERS = (1.0 / ENCODER_CPR) * (1.0 / GEAR_RATIO) * WHEEL_CIRCUMFERENCE;

        public static final double TRACK_WIDTH_METERS = 1.2529241489626137;
        public static final DifferentialDriveKinematics DRIVE_KINEMATICS = new DifferentialDriveKinematics(
                TRACK_WIDTH_METERS);

        public static final int LIL_TIMEOUT = 10;
        public static final int BIG_TIMEOUT = 50;
        public static final double OPEN_LOOP_RAMP = 0.5; // how many seconds it takes to go from neutral to full output

        public static final int LEFT_MASTER = 1;
        public static final int LEFT_SLAVE = 2;
        public static final int RIGHT_MASTER = 3;
        public static final int RIGHT_SLAVE = 4;

        public static final boolean kGyroReversed = true;
        public static final boolean SENSOR_PHASE = true;
        public static final boolean INVERTED = false;
        public static final int PID_SLOT = 0;

        public static final double VOLTAGE_COMP_SATURATION = 12;
        public static final boolean VOLTAGE_COMP_ENABLED = false;

        // will need characterization to find values
        // tuned to carpet on 4/5/21
        public static final double S_VOLTS = 0.777;
        public static final double V_VOLTS_SECOND_PER_METER = 1.48;
        public static final double A_VOLT_SEONDS_SQUARED_PER_METER = 0.111;

        public static final double MAX_SPEED_METERS_PER_SECOND = Units.feetToMeters(4.0);
        public static final double MAX_ACCELERATION_METERS_PER_SECOND_SQUARED = Units.feetToMeters(1.0);
        public static final double P_DRIVE_VEL = 2.16;

        public static final double MAX_VOLTAGE = 11;
        public static final DifferentialDriveVoltageConstraint autoVoltageConstraint = new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(S_VOLTS, V_VOLTS_SECOND_PER_METER, A_VOLT_SEONDS_SQUARED_PER_METER),
                DRIVE_KINEMATICS, MAX_VOLTAGE);
        public static final TrajectoryConfig CONFIG = new TrajectoryConfig(MAX_SPEED_METERS_PER_SECOND,
                MAX_ACCELERATION_METERS_PER_SECOND_SQUARED).setKinematics(DRIVE_KINEMATICS)
                        .addConstraint(autoVoltageConstraint);

    }

    public static final class kFlywheel {
        public static final int MOTOR = 6;
        public static final boolean INVERTED = false;
        // tuned to flywheel on 3/1/21
        public static final double kV = 0.0207;
        public static final double kA = 0.00459;

        public static final int[] PISTONS = { 2, 3 }; // assign later
    }

    public static final class kIntake {
        public static final int LEFT_MOTOR = 6; // assign later
        public static final int RIGHT_MOTOR = 7; // assign later
        public static final double SPEED = 0.5; // volts out of a maximum of 12
        public static final boolean REVERSED = false; // assign later

        public static final int[] PISTONS = { 1, 0 }; // assign later
    }

    public static final class kIndexer {
        public static final int MOTOR_A = -1; // assign later
        public static final int MOTOR_B = -1; // assign later
        public static final int SPEED = 4; // volts out of a maximum of 12
        public static final boolean REVERSED = false; // assign later
    }

    public static final class kLimelight {
        public static final double a1 = 21.0; // mounting angle
        public static final double h1 = 2.125; // height of your camera above the floor (in feet)
        public static final double h2 = 7.0; // height of the target (in feet)
    }

    public static final class kJoySticks {
        public static final int LEFT = 0;
        public static final int RIGHT = 1;

        public static final int POV_UP = 0;
        public static final int POV_RIGHT = 90;
        public static final int POV_DOWN = 180;
        public static final int POV_LEFT = 270;
    }

}
