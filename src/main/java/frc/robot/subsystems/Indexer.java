// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kIndexer;

public class Indexer extends SubsystemBase {
  private final WPI_TalonSRX motorA, motorB;

  /** Creates a new Indexer. */
  public Indexer() {
    motorA = new WPI_TalonSRX(kIndexer.MOTOR_A);
    motorB = new WPI_TalonSRX(kIndexer.MOTOR_B);

    motorA.setInverted(kIndexer.REVERSED);
    motorB.follow(motorA);
    motorB.setInverted(InvertType.FollowMaster);
  }

  public void run() {
    motorA.setVoltage(kIndexer.SPEED);
  }

  public void reverse() {
    motorA.setVoltage(-kIndexer.SPEED);
  }

  public void stop() {
    motorA.stopMotor();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
