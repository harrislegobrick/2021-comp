// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kIntake;

public class Intake extends SubsystemBase {
  private final CANSparkMax leftMotor, rightMotor;
  private final DoubleSolenoid pistons;

  /** Creates a new Intake. */
  public Intake() {
    pistons = new DoubleSolenoid(kIntake.PISTONS[0], kIntake.PISTONS[1]);
    stow();

    leftMotor = new CANSparkMax(kIntake.LEFT_MOTOR, MotorType.kBrushless);
    rightMotor = new CANSparkMax(kIntake.RIGHT_MOTOR, MotorType.kBrushless);

    rightMotor.setInverted(kIntake.REVERSED);
    leftMotor.setInverted(!kIntake.REVERSED);
  }

  public void run() {
    rightMotor.set(kIntake.SPEED);
    leftMotor.set(kIntake.SPEED);
  }

  public void stop() {
    rightMotor.stopMotor();
    leftMotor.stopMotor();
  }

  public void drop() {
    pistons.set(Value.kForward);
  }

  public void stow() {
    pistons.set(Value.kReverse);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
