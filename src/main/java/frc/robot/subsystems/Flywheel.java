// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANEncoder;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.controller.LinearQuadraticRegulator;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.estimator.KalmanFilter;
import edu.wpi.first.wpilibj.system.plant.LinearSystemId;
import edu.wpi.first.wpilibj.system.LinearSystemLoop;
import edu.wpi.first.wpilibj.system.LinearSystem;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpiutil.math.VecBuilder;
import edu.wpi.first.wpiutil.math.numbers.N1;
import edu.wpi.first.wpiutil.math.Nat;
import frc.robot.Constants.kFlywheel;
import frc.robot.Robot;

public class Flywheel extends SubsystemBase {
    private final CANSparkMax motor;
    private final CANEncoder encoder;
    private final DoubleSolenoid pistons;

    // The plant holds a state-space model of our flywheel. This system has the
    // following properties:
    // States: [velocity], in radians per second.
    // Inputs (what we can "put in"): [voltage], in volts.
    // Outputs (what we can measure): [velocity], in radians per second.
    private final LinearSystem<N1, N1, N1> flywheelPlant = LinearSystemId.identifyVelocitySystem(kFlywheel.kV,
            kFlywheel.kA);

    private final KalmanFilter<N1, N1, N1> observer = new KalmanFilter<>(Nat.N1(), Nat.N1(), flywheelPlant,
            VecBuilder.fill(3.0), // how accurate we think our model is
            VecBuilder.fill(0.01), // how accurate we think our encoder data is
            Robot.kDefaultPeriod);

    private final LinearQuadraticRegulator<N1, N1, N1> controller = new LinearQuadraticRegulator<>(flywheelPlant,
            VecBuilder.fill(8.0), // qelms. Velocity error tolerance, in radians per second. Decrease this to more
                                  // heavily penalize state excursion, or make the controller behave more
                                  // aggressively
            VecBuilder.fill(12.0), // relms. Control effort (voltage) tolerance. Decrease this to more
            // heavily penalize control effort, or make the controller less aggressive. 12
            // is a good starting point because that is the (approximate) maximum voltage of
            // a battery.
            Robot.kDefaultPeriod); // Nominal time between loops. 0.020 for TimedRobot, but can be lower if using
                                   // notifiers.

    // The state-space loop combines a controller, observer, feedforward and plant
    // for easy control.
    private final LinearSystemLoop<N1, N1, N1> loop = new LinearSystemLoop<>(flywheelPlant, controller, observer, 12.0,
            Robot.kDefaultPeriod);

    /**
     * Creates a new Flywheel.
     */
    public Flywheel() {
        pistons = new DoubleSolenoid(kFlywheel.PISTONS[0], kFlywheel.PISTONS[1]);
        extend();

        motor = new CANSparkMax(kFlywheel.MOTOR, MotorType.kBrushless);
        encoder = motor.getEncoder();

        motor.restoreFactoryDefaults();
        motor.setIdleMode(IdleMode.kBrake);
        motor.setInverted(kFlywheel.INVERTED);
        motor.burnFlash();

        // Adjust our LQR's controller for 25 ms of sensor input delay. We
        // provide the linear system, discretization timestep, and the sensor
        // input delay as arguments.
        controller.latencyCompensate(flywheelPlant, Robot.kDefaultPeriod, 0.025);
    }

    /**
     * 4000 RPM is a good value for launching from 15 ft away.
     * 
     * @param rpm : The rpm to set the motor to.
     */
    public void setRPM(double rpm) {
        loop.setNextR(VecBuilder.fill(Units.rotationsPerMinuteToRadiansPerSecond(rpm)));
    }

    public double getVelocity() {
        return encoder.getVelocity();
    }

    public void stop() {
        loop.setNextR(VecBuilder.fill(0.0));
    }

    public void retract() {
        pistons.set(Value.kReverse);
    }

    public void extend() {
        pistons.set(Value.kForward);
    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run

        // Correct our Kalman filter's state vector estimate with encoder data.
        loop.correct(VecBuilder.fill(Units.rotationsPerMinuteToRadiansPerSecond(getVelocity())));
        // Update our LQR to generate new voltage commands and use the voltages to
        // predict the next state with out Kalman filter.
        loop.predict(Robot.kDefaultPeriod);

        // sets motor voltage to the loop's calculation
        motor.setVoltage(loop.getU(0));

        // observe motor rpm and filtered rpm
        SmartDashboard.putNumber("Kalman filter RPM", Units.radiansPerSecondToRotationsPerMinute(loop.getXHat(0)));
        SmartDashboard.putNumber("motor RPM", getVelocity());
    }
}