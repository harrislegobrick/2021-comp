// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.kDrivetrain;

public class Drivetrain extends SubsystemBase {
  private final WPI_TalonSRX leftCommander, rightCommander;
  private final VictorSPX leftFollower, rightFollower;
  private final DifferentialDriveOdometry odometry;
  private final Field2d field;
  private final AHRS gyro;

  /**
   * Creates a new Drivetrain.
   */
  public Drivetrain() {
    leftCommander = new WPI_TalonSRX(kDrivetrain.LEFT_MASTER);
    leftFollower = new VictorSPX(kDrivetrain.LEFT_SLAVE);
    rightCommander = new WPI_TalonSRX(kDrivetrain.RIGHT_MASTER);
    rightFollower = new VictorSPX(kDrivetrain.RIGHT_SLAVE);
    field = new Field2d();

    gyro = new AHRS();

    odometry = new DifferentialDriveOdometry(Rotation2d.fromDegrees(getHeading()));

    // reset talons
    leftCommander.configFactoryDefault(kDrivetrain.LIL_TIMEOUT);
    leftFollower.configFactoryDefault(kDrivetrain.LIL_TIMEOUT);
    rightCommander.configFactoryDefault(kDrivetrain.LIL_TIMEOUT);
    rightFollower.configFactoryDefault(kDrivetrain.LIL_TIMEOUT);

    // slaves follow the masters
    leftFollower.follow(leftCommander);
    rightFollower.follow(rightCommander);

    // invert the motors so that positive is forward
    leftCommander.setInverted(kDrivetrain.INVERTED);
    rightCommander.setInverted(!kDrivetrain.INVERTED);
    leftFollower.setInverted(InvertType.FollowMaster);
    rightFollower.setInverted(InvertType.FollowMaster);

    // encoder setup
    leftCommander.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, kDrivetrain.PID_SLOT,
        kDrivetrain.LIL_TIMEOUT);
    rightCommander.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, kDrivetrain.PID_SLOT,
        kDrivetrain.LIL_TIMEOUT);
    leftCommander.setSensorPhase(kDrivetrain.SENSOR_PHASE);
    rightCommander.setSensorPhase(kDrivetrain.SENSOR_PHASE);

    // ramping
    leftCommander.configOpenloopRamp(kDrivetrain.OPEN_LOOP_RAMP);
    rightCommander.configOpenloopRamp(kDrivetrain.OPEN_LOOP_RAMP);

    // voltage compensation
    leftCommander.configVoltageCompSaturation(kDrivetrain.VOLTAGE_COMP_SATURATION);
    leftCommander.enableVoltageCompensation(kDrivetrain.VOLTAGE_COMP_ENABLED);
    rightCommander.configVoltageCompSaturation(kDrivetrain.VOLTAGE_COMP_SATURATION);
    rightCommander.enableVoltageCompensation(kDrivetrain.VOLTAGE_COMP_ENABLED);

    // zero sensors
    resetEncoders();
    resetGyro();

    SmartDashboard.putData(gyro);
    SmartDashboard.putData(field);
  }

  /**
   * Returns the current wheel speeds of the robot.
   *
   * @return The current wheel speeds.
   */
  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(leftCommander.getSelectedSensorVelocity() * kDrivetrain.TICKS_TO_METERS
    /* multiply by 10 to go from ms to s */ * 10.0,
        rightCommander.getSelectedSensorVelocity() * kDrivetrain.TICKS_TO_METERS
        /* multiply by 10 to go from ms to s */ * 10.0);
  }

  public void resetEncoders() {
    leftCommander.setSelectedSensorPosition(0);
    rightCommander.setSelectedSensorPosition(0);
  }

  public void drive(double left, double right) {
    leftCommander.set(ControlMode.PercentOutput, left);
    rightCommander.set(ControlMode.PercentOutput, right);
  }

  public void driveVolts(double leftVolts, double rightVolts) {
    leftCommander.setVoltage(leftVolts);
    rightCommander.setVoltage(rightVolts);
  }

  public void resetGyro() {
    gyro.zeroYaw();
  }

  public void stop() {
    rightCommander.set(ControlMode.PercentOutput, 0);
    leftCommander.set(ControlMode.PercentOutput, 0);
  }

  /**
   * Returns the heading of the robot.
   *
   * @return the robot's heading in degrees, from 180 to 180
   */
  public double getHeading() {
    return Math.IEEEremainder(gyro.getAngle(), 360) * (kDrivetrain.kGyroReversed ? -1.0 : 1.0);
  }

  public Pose2d getPose() {
    return odometry.getPoseMeters();
  }

  public void setPosition(Pose2d poseMeters) {
    resetEncoders();
    odometry.resetPosition(poseMeters, Rotation2d.fromDegrees(getHeading()));
  }

  public void setPosition(double x, double y, double degrees) {
    resetEncoders();
    odometry.resetPosition(new Pose2d(x, y, Rotation2d.fromDegrees(degrees)), Rotation2d.fromDegrees(getHeading()));
  }

  public Field2d getField2d() {
    return field;
  }

  @Override
  public void periodic() {
    odometry.update(Rotation2d.fromDegrees(getHeading()),
        leftCommander.getSelectedSensorPosition() * kDrivetrain.TICKS_TO_METERS,
        rightCommander.getSelectedSensorPosition() * kDrivetrain.TICKS_TO_METERS);
    field.setRobotPose(getPose());
  }
}