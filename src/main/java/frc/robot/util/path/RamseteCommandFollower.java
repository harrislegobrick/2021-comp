// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.util.path;

import java.util.stream.Collectors;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.robot.Constants.kDrivetrain;
import frc.robot.subsystems.Drivetrain;

public class RamseteCommandFollower extends RamseteCommand {
  private final Drivetrain drivetrain;
  private final Trajectory trajectory;
  private boolean startAtTrajectoryStart = true;
  private boolean stopAtEnd = true;

  /**
   * A command that will follow a trajectory.
   * <p>
   * There are optional settings to change whether it should start at the
   * trajectory's start
   * <ul>
   * <li>{@link RamseteCommandFollower#startAtTrajectoryStart(boolean)} to
   * determine whether it should start at the trajectory's inital pose.</li>
   * <li>{@link RamseteCommandFollower#stopAtEnd(boolean)} to determine whether
   * the robot should stop at the end of the trajectory.</li>
   * <li>{@link RamseteCommandFollower#drawTrajectory()} to draw the trajectory to
   * the Glass dashboard.</li>
   * </ul>
   * 
   * @param trajectory the trajectory to follow
   * @param drivetrain the drivetrain to drive the path
   */
  public RamseteCommandFollower(Trajectory trajectory, Drivetrain drivetrain) {
    super(trajectory, drivetrain::getPose, new RamseteController(),
        new SimpleMotorFeedforward(kDrivetrain.S_VOLTS, kDrivetrain.V_VOLTS_SECOND_PER_METER,
            kDrivetrain.A_VOLT_SEONDS_SQUARED_PER_METER),
        kDrivetrain.DRIVE_KINEMATICS, drivetrain::getWheelSpeeds, new PIDController(kDrivetrain.P_DRIVE_VEL, 0, 0),
        new PIDController(kDrivetrain.P_DRIVE_VEL, 0, 0), drivetrain::driveVolts, drivetrain);
    this.drivetrain = drivetrain;
    this.trajectory = trajectory;
  }

  /**
   * Draws the trajectory to Glass.
   * 
   * @return The <code>RamseteCommand</code> this method is being called on.
   */
  public RamseteCommandFollower drawTrajectory() {
    drivetrain.getField2d().getObject(String.format("Trajectory - %.2fs", trajectory.getTotalTimeSeconds()))
        .setPoses(trajectory.getStates().stream().map(state -> state.poseMeters).collect(Collectors.toList()));
    return this;
  }

  /**
   * Only ever need to set to <code>false</code> if this trajectory is after the
   * first one because it is <code>true</code> by default.
   * 
   * @param startAtTrajectoryStart Sets whether the robot should reset its pose to
   *                               the path's inital pose.
   * @return The <code>RamseteCommand</code> this method is being called on.
   */
  public RamseteCommandFollower startAtTrajectoryStart(boolean startAtTrajectoryStart) {
    this.startAtTrajectoryStart = startAtTrajectoryStart;
    return this;
  }

  /**
   * Only ever needed to set to <code>false</code> if the ending velocity of the
   * path is not 0 because it is <code>true</code> by default.
   * 
   * @param stopAtEnd Sets whether the robot should stop at the end of the path.
   * @return The <code>RamseteCommand</code> this method is being called on.
   */
  public RamseteCommandFollower stopAtEnd(boolean stopAtEnd) {
    this.stopAtEnd = stopAtEnd;
    return this;
  }

  @Override
  public void initialize() {
    if (startAtTrajectoryStart) {
      drivetrain.setPosition(trajectory.getInitialPose());
    }
    super.initialize();
  }

  @Override
  public void end(boolean interrupted) {
    super.end(interrupted);
    if (stopAtEnd) {
      drivetrain.stop();
    }
  }
}
