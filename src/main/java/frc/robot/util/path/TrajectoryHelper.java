package frc.robot.util.path;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.spline.Spline.ControlVector;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator.ControlVectorList;

/**
 * Class containing methods to help with generating trajectories from Pathweaver
 * paths in the deploy/paths directory.
 */
public class TrajectoryHelper {
    private static final double PATHWEAVER_Y_OFFSET = 4.572; // See https://www.desmos.com/calculator/0lqfdhxrmj

    private TrajectoryHelper() {
    }

    /**
     * Get control vector list from path file
     * 
     * @param pathName Specify the {THIS} in src/main/deploy/waypoints/{THIS}.path
     * @return control vectors from file
     * @throws FileNotFoundException
     */
    public static ControlVectorList getControlVectors(String pathName) throws IOException {
        String relativePath = "paths/" + pathName;
        Path path = Filesystem.getDeployDirectory().toPath().resolve(relativePath);

        ControlVectorList controlVectors = new ControlVectorList();

        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
            reader.lines().skip(1).forEachOrdered(line -> {
                String[] split = line.split(",");
                controlVectors.add(new ControlVector(
                        new double[] { Double.parseDouble(split[0]), Double.parseDouble(split[2]), 0 }, new double[] {
                                Double.parseDouble(split[1]) + PATHWEAVER_Y_OFFSET, Double.parseDouble(split[3]), 0 }));
            });
        }

        // Debug print out of arrays in vectors
        // for (Spline.ControlVector vector : controlVectors) {
        // System.out.println(Arrays.toString(vector.x));
        // System.out.println(Arrays.toString(vector.y));
        // System.out.println("===");
        // }

        return controlVectors;
    }

    /**
     * @param pathName File name in the paths folder.
     * @param config   A trajectory config for the path to use.
     * @return A trajectory.
     */
    public static Trajectory generateTrajectory(String pathName, TrajectoryConfig config) {
        try {
            return TrajectoryGenerator.generateTrajectory(getControlVectors(pathName), config);
        } catch (Exception e) {
            System.err.printf("%nFailed to generate custom trajectory for path name %s!!%n%n", pathName);
            return null;
        }
    }
}