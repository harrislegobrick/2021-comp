// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.ArrayList;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj2.command.button.POVButton;
import frc.robot.Constants.kJoySticks;
import frc.robot.commands.IntakeCommand;
import frc.robot.commands.ShootCommand;
import frc.robot.commands.TankDrive;
import frc.robot.commands.autons.BarrelRacingPath;
import frc.robot.commands.autons.BounceRun;
import frc.robot.commands.autons.SlalomPath;
import frc.robot.commands.autons.TestPath;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Limelight;
import frc.robot.util.path.RamseteCommandFollower;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // Autonomous selector which allows us to choose which autonomous command to run
  private final SendableChooser<Command> chooser = new SendableChooser<Command>();

  // joysticks
  private final Joystick lJoy = new Joystick(kJoySticks.LEFT);
  private final Joystick rJoy = new Joystick(kJoySticks.RIGHT);

  // The robot's subsystems and commands are defined here...
  private final Drivetrain drivetrain = new Drivetrain();
  private final Limelight limelight = new Limelight();
  private final Flywheel flywheel = new Flywheel();
  private final Intake intake = new Intake();
  private final Indexer indexer = new Indexer();

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    drivetrain.setDefaultCommand(new TankDrive(lJoy::getY, rJoy::getY, rJoy::getThrottle, drivetrain));

    // Adds and sets autonomous actions to the smartdashboard for the user to select
    chooser.setDefaultOption("Choose an Autonomous Command", new PrintCommand("Select a command!!"));
    chooser.addOption("Test Path", new TestPath(drivetrain));
    chooser.addOption("Barrel Racing Path", new BarrelRacingPath(drivetrain));
    chooser.addOption("Bounce Path", new BounceRun(drivetrain));
    chooser.addOption("Slalom Path", new SlalomPath(drivetrain));
    SmartDashboard.putData("Auto Mode", chooser);

    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by instantiating a {@link GenericHID} or one of its subclasses
   * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
   * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    // left stick
    new POVButton(lJoy, kJoySticks.POV_DOWN).whenPressed(intake::drop, intake); // deploys intake
    new POVButton(lJoy, kJoySticks.POV_UP).whenPressed(intake::stow, intake); // retracts intake
    new JoystickButton(lJoy, 1).whileActiveOnce(new IntakeCommand(intake, indexer)); // runs intake motor and indexer
    // right stick
    new POVButton(rJoy, kJoySticks.POV_UP).whenPressed(flywheel::extend, flywheel); // extends hood
    new POVButton(rJoy, kJoySticks.POV_DOWN).whenPressed(flywheel::retract, flywheel); // retracts hood
    new JoystickButton(rJoy, 1).whileActiveOnce(new ShootCommand(flywheel, limelight, indexer)); // runs the indexer
                                                                                                 // and flywheel

    new JoystickButton(rJoy, 2).whenPressed(() -> {
      ArrayList<Translation2d> interiorWaypoints = new ArrayList<Translation2d>();
      interiorWaypoints.add(new Translation2d(2, 1));
      interiorWaypoints.add(new Translation2d(4, 1));
      
      Pose2d end = new Pose2d(12, 2, Rotation2d.fromDegrees(0));
      Trajectory trajectory = TrajectoryGenerator.generateTrajectory(drivetrain.getPose(), interiorWaypoints, end,
          Constants.kDrivetrain.CONFIG);
      
      new RamseteCommandFollower(trajectory, drivetrain).startAtTrajectoryStart(false).schedule(true);
    }).whenInactive(drivetrain::stop, drivetrain);

    new JoystickButton(rJoy, 3).whenPressed(() -> drivetrain.setPosition(2, 12, 0));
    new JoystickButton(rJoy, 4).whenPressed(() -> drivetrain.setPosition(12, 4, 0));
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    return chooser.getSelected();
  }
}
